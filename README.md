# nuScenes detection task

The goal of this task is to place a 3D bounding box around at least one of the object categories existing in the vehicle surrounding.

Details and further references can be found at [https://www.nuscenes.org/object-detection](https://www.nuscenes.org/object-detection).

Task utilizes neural networks trained on the labeled nuScene dataset (to be downloaded from [https://www.nuscenes.org/nuscenes#download](https://www.nuscenes.org/nuscenes#download)).

The project will be documented in a form of a research paper.

Plan is to benchmark the performance of our method against the community taking part in the nuScene challenge (vision track).

Result on a short fragment of a benchmark video:

<img src="./img/video_gif.gif"/>

## Requirements
 - `Tensorflow >= 2.5`
 - Tensorflow Object Detection API: `object-detection`
 - `tensorflow-hub`
 - `pillow`
 - `six`


## Authors and acknowledgment
Project started as a AIAV: project classes subject.

Authors:

- Michal Piekarski
- Andrzej Brodzicki

## License
For a Licence see `LICENSE` file.

