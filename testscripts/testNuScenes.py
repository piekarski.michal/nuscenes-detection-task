import matplotlib.pyplot as plt
import numpy as np
from nuscenes.nuscenes import NuScenes
from nuscenes.utils.geometry_utils import view_points
from nuscenes.scripts.export_2d_annotations_as_json import post_process_coords
from pyquaternion.quaternion import Quaternion

SENSOR = "CAM_FRONT"

# Visibility bins, the higher the number the higher the visibility.
visibilities = ["", "1", "2", "3", "4"]


def plot_sensor_category(category="vehicle.car"):
    CATEGORY = category
    nusc = NuScenes(version="v1.0-mini", dataroot="dataset", verbose=True)
    my_scene = nusc.scene[0]
    sample_token = my_scene["first_sample_token"]
    my_sample = nusc.get("sample", sample_token)

    # Get the sample data and the sample corresponding to that sample data.
    sd_rec = nusc.get("sample_data", my_sample["data"][SENSOR])
    s_rec = nusc.get("sample", sd_rec["sample_token"])

    # Get the calibrated sensor and ego pose record to get the transformation matrices.
    cs_rec = nusc.get("calibrated_sensor", sd_rec["calibrated_sensor_token"])
    pose_rec = nusc.get("ego_pose", sd_rec["ego_pose_token"])
    camera_intrinsic = np.array(cs_rec["camera_intrinsic"])
    print(sd_rec["filename"])
    im = plt.imread(f"dataset/{sd_rec['filename']}")

    # Get all the annotation with the specified visibilties.
    ann_recs = [nusc.get("sample_annotation", token) for token in s_rec["anns"]]
    ann_recs = [
        ann_rec for ann_rec in ann_recs if (ann_rec["visibility_token"] in visibilities)
    ]

    fig, ax = plt.subplots(1, 1)

    for ann_rec in ann_recs:
        # Augment sample_annotation with token information.
        ann_rec["sample_annotation_token"] = ann_rec["token"]
        ann_rec["sample_data_token"] = my_sample["data"][SENSOR]

        # Get the box in global coordinates.
        box = nusc.get_box(ann_rec["token"])

        # Move them to the ego-pose frame.
        box.translate(-np.array(pose_rec["translation"]))
        box.rotate(Quaternion(pose_rec["rotation"]).inverse)

        # Move them to the calibrated sensor frame.
        box.translate(-np.array(cs_rec["translation"]))
        box.rotate(Quaternion(cs_rec["rotation"]).inverse)

        # Filter out the corners that are not in front of the calibrated sensor.
        corners_3d = box.corners()
        corners = view_points(corners_3d, camera_intrinsic, normalize=True)[:2, :]

        in_front2 = np.argwhere(corners_3d[2, :] > 0).flatten()
        corners_3d2 = corners_3d[:, in_front2]
        corner_coords = view_points(corners_3d2, camera_intrinsic, True).T[:, :2].tolist()
        final_coords = post_process_coords(corner_coords)

        visible = np.logical_and(corners[0, :] > 0, corners[0, :] < im.shape[1])
        visible = np.logical_and(visible, corners[1, :] < im.shape[0])
        visible = np.logical_and(visible, corners[1, :] > 0)
        visible = np.logical_and(visible, corners_3d[2, :] > 1)

        in_front = corners_3d[2, :] > 0.1

        final_vis = all(visible) and all(in_front)

        if final_vis and ann_rec["category_name"] == CATEGORY:

            def draw_rect(selected_corners):
                prev = selected_corners[-1]
                for corner in selected_corners:
                    ax.plot(
                        [prev[0], corner[0]],
                        [prev[1], corner[1]],
                        color="r",
                        linewidth=2,
                    )
                    prev = corner

            for i in range(4):
                ax.plot(
                    [corners.T[i][0], corners.T[i + 4][0]],
                    [corners.T[i][1], corners.T[i + 4][1]],
                    color="r",
                    linewidth=2,
                )

            draw_rect(corners.T[:4])
            draw_rect(corners.T[4:])

            center_bottom_forward = np.mean(corners.T[2:4], axis=0)
            center_bottom = np.mean(corners.T[[2, 3, 7, 6]], axis=0)
            ax.plot(
                [center_bottom[0], center_bottom_forward[0]],
                [center_bottom[1], center_bottom_forward[1]],
                color="r",
                linewidth=2,
            )
    ax.imshow(im)
    plt.show()


if __name__ == "__main__":
    """
    Categories:
    human.pedestrian.adult      n= 4765, width= 0.68±0.11, len= 0.73±0.17, height= 1.76±0.12, lw_aspect= 1.08±0.23
    human.pedestrian.child      n=   46, width= 0.46±0.08, len= 0.45±0.09, height= 1.37±0.06, lw_aspect= 0.97±0.05
    human.pedestrian.constructi n=  193, width= 0.69±0.07, len= 0.74±0.12, height= 1.78±0.05, lw_aspect= 1.07±0.16
    human.pedestrian.personal_m n=   25, width= 0.83±0.00, len= 1.28±0.00, height= 1.87±0.00, lw_aspect= 1.55±0.00
    human.pedestrian.police_off n=   11, width= 0.59±0.00, len= 0.47±0.00, height= 1.81±0.00, lw_aspect= 0.80±0.00
    movable_object.barrier      n= 2323, width= 2.32±0.49, len= 0.61±0.11, height= 1.06±0.10, lw_aspect= 0.28±0.09
    movable_object.debris       n=   13, width= 0.43±0.00, len= 1.43±0.00, height= 0.46±0.00, lw_aspect= 3.35±0.00
    movable_object.pushable_pul n=   82, width= 0.51±0.06, len= 0.79±0.10, height= 1.04±0.20, lw_aspect= 1.55±0.18
    movable_object.trafficcone  n= 1378, width= 0.47±0.14, len= 0.45±0.07, height= 0.78±0.13, lw_aspect= 0.99±0.12
    static_object.bicycle_rack  n=   54, width= 2.67±1.46, len=10.09±6.19, height= 1.40±0.00, lw_aspect= 5.97±4.02
    vehicle.bicycle             n=  243, width= 0.64±0.12, len= 1.82±0.14, height= 1.39±0.34, lw_aspect= 2.94±0.41
    vehicle.bus.bendy           n=   57, width= 2.83±0.09, len= 9.23±0.33, height= 3.32±0.07, lw_aspect= 3.27±0.22
    vehicle.bus.rigid           n=  353, width= 2.95±0.26, len=11.46±1.79, height= 3.80±0.62, lw_aspect= 3.88±0.57
    vehicle.car                 n= 7619, width= 1.92±0.16, len= 4.62±0.36, height= 1.69±0.21, lw_aspect= 2.41±0.18
    vehicle.construction        n=  196, width= 2.58±0.35, len= 5.57±1.57, height= 2.38±0.33, lw_aspect= 2.18±0.62
    vehicle.motorcycle          n=  471, width= 0.68±0.21, len= 1.95±0.38, height= 1.47±0.20, lw_aspect= 3.00±0.62
    vehicle.trailer             n=   60, width= 2.28±0.08, len=10.14±5.69, height= 3.71±0.27, lw_aspect= 4.37±2.41
    vehicle.truck               n=  649, width= 2.35±0.34, len= 6.50±1.56, height= 2.62±0.68, lw_aspect= 2.75±0.37
    """
    plot_sensor_category(category="vehicle.truck")
