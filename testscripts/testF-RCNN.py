# This requires TF 2.5
# Clone the tensorflow models repository
# git clone --depth 1 https://github.com/tensorflow/models


import time

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
from object_detection.utils import visualization_utils as viz_utils
from PIL import Image
from six import BytesIO
from six.moves.urllib.request import urlopen

COCO17_HUMAN_POSE_KEYPOINTS = [
    (0, 1),
    (0, 2),
    (1, 3),
    (2, 4),
    (0, 5),
    (0, 6),
    (5, 7),
    (7, 9),
    (6, 8),
    (8, 10),
    (5, 6),
    (5, 11),
    (6, 12),
    (11, 12),
    (11, 13),
    (13, 15),
    (12, 14),
    (14, 16),
]

tf.get_logger().setLevel("ERROR")

MODEL = "https://tfhub.dev/tensorflow/faster_rcnn/inception_resnet_v2_640x640/1"
# IMG_PTH = "./dataset/samples/CAM_FRONT/n008-2018-08-01-15-16-36-0400__CAM_FRONT__1533151606012404.jpg"
IMG_PTH = "./dataset/samples/CAM_FRONT/n015-2018-07-24-11-22-45+0800__CAM_FRONT__1532402927612460.jpg"
PATH_TO_LABELS = "S:/PycharmProjects/nuscenes-detection-task/models/research/object_detection/data/mscoco_label_map.pbtxt"
category_index = label_map_util.create_category_index_from_labelmap(
    PATH_TO_LABELS, use_display_name=True
)

MIN_SCORE = 0.8


def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.

    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.

    Args:
      path: the file path to the image

    Returns:
      uint8 numpy array with shape (img_height, img_width, 3)
    """
    image = None
    if path.startswith("http"):
        response = urlopen(path)
        image_data = response.read()
        image_data = BytesIO(image_data)
        image = Image.open(image_data)
    else:
        image_data = tf.io.gfile.GFile(path, "rb").read()
        image = Image.open(BytesIO(image_data))

    (im_width, im_height) = image.size
    return (
        np.array(image.getdata()).reshape((1, im_height, im_width, 3)).astype(np.uint8)
    )


def main():
    model_display_name = "Faster R-CNN Inception ResNet V2 640x640"
    print("Selected model:" + model_display_name)
    print("Model Handle at TensorFlow Hub: {}".format(MODEL))
    print("loading model...")
    hub_model = hub.load(MODEL)
    print("model loaded!")
    image_path = IMG_PTH
    image_np = load_image_into_numpy_array(image_path)
    # running inference
    t = time.time()
    results = hub_model(image_np)
    print("Elapsed time: ", time.time() - t)
    # different object detection models have additional results
    # all of them are explained in the documentation
    result = {key: value.numpy() for key, value in results.items()}
    label_id_offset = 0
    image_np_with_detections = image_np.copy()

    # Use keypoints if available in detections
    keypoints, keypoint_scores = None, None
    if "detection_keypoints" in result:
        keypoints = result["detection_keypoints"][0]
        keypoint_scores = result["detection_keypoint_scores"][0]

    viz_utils.visualize_boxes_and_labels_on_image_array(
        image_np_with_detections[0],
        result["detection_boxes"][0],
        (result["detection_classes"][0] + label_id_offset).astype(int),
        result["detection_scores"][0],
        category_index,
        use_normalized_coordinates=True,
        max_boxes_to_draw=200,
        min_score_thresh=MIN_SCORE,
        agnostic_mode=False,
        keypoints=keypoints,
        keypoint_scores=keypoint_scores,
        keypoint_edges=COCO17_HUMAN_POSE_KEYPOINTS,
    )
    plt.imsave("res.png", image_np_with_detections[0])
    score_threshed = result["detection_scores"][0] >= MIN_SCORE
    res = np.where(score_threshed == 1)[0]
    im_size = image_np.shape
    ann = []
    for el in res:
        cat = result["detection_classes"][0][el]
        cat_name = category_index[cat]["name"].replace(" ", "")
        xmin = result["detection_boxes"][0][el][1] * im_size[2]
        ymin = result["detection_boxes"][0][el][0] * im_size[1]
        xmax = result["detection_boxes"][0][el][3] * im_size[2]
        ymax = result["detection_boxes"][0][el][2] * im_size[1]
        score = result["detection_scores"][0][el]
        ann.append(
            f"{cat_name} {xmin} {ymin} {xmax} {ymax} {score}"
        )

    # with open('results2.txt', 'w') as f:
    #     f.write('\n'.join(ann))


if __name__ == "__main__":
    main()
