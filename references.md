# Useful links:

## Projects:

https://github.com/poodarchu/Det3D

https://github.com/xinshuoweng/AB3DMOT

https://github.com/traveller59/second.pytorch

https://github.com/traveller59/second.pytorch/blob/master/NUSCENES-GUIDE.md

https://github.com/Socret360/object-detection-in-keras

https://github.com/ManishSoni1908/Mobilenet-ssd-keras

https://github.com/qfgaohao/pytorch-ssd

https://github.com/nutonomy/nuscenes-devkit

https://github.com/facebookresearch/detectron2

https://github.com/kuangliu/pytorch-retinanet

## Others

[nuScenes devkit tutorial](https://colab.research.google.com/github/nutonomy/nuscenes-devkit/blob/master/python-sdk/tutorials/nuscenes_tutorial.ipynb) 

[Objectron](https://google.github.io/mediapipe/solutions/objectron.html)

[nuScenes](https://www.nuscenes.org/nuscenes?tutorial=nuscenes)

[TF models HUB](https://github.com/tensorflow/models)