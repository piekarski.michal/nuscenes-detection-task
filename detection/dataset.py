import logging
import sys

import matplotlib.pyplot as plt
import numpy as np
from nuscenes.nuscenes import NuScenes
from nuscenes.scripts.export_2d_annotations_as_json import post_process_coords
from nuscenes.utils.geometry_utils import view_points
from pyquaternion.quaternion import Quaternion

# Visibility bins, the higher the number the higher the visibility.
visibilities = ["", "1", "2", "3", "4"]


class Dataset:
    def __init__(
        self,
        scene_ind=0,
        NuScenes_ver="v1.0-mini",
        log_level=logging.DEBUG,
        category="vehicle.car",
        sensor="CAM_FRONT",
    ):
        self.scene_ind = scene_ind
        self.sensor = sensor
        self.nuscenes_ver = NuScenes_ver
        self.category = category
        self.logger = logging.getLogger("tipper")
        self.logger.addHandler(logging.StreamHandler(sys.stdout))
        self.logger.setLevel(log_level)
        self.dataset = None
        self.scene = None
        self.sample_token = None
        self.sample = None
        self.sample_data = None
        self.last_token = None
        self.filename = None
        self.init_dataset()

    def init_dataset(self):
        self.dataset = NuScenes(version="v1.0-mini", dataroot="dataset", verbose=True)
        self.scene = self.dataset.scene[self.scene_ind]
        self.sample_token = self.scene["first_sample_token"]
        self.last_token = self.scene["last_sample_token"]
        self.get_sample_and_data()

    def next_sample(self):
        self.sample_token = self.sample["next"]
        self.get_sample_and_data()

    def get_sample_and_data(self):
        self.sample = self.dataset.get("sample", self.sample_token)
        self.sample_data = self.dataset.get(
            "sample_data", self.sample["data"][self.sensor]
        )
        self.filename = self.sample_data["filename"]

    def iterate(self):
        cs_rec = self.dataset.get(
            "calibrated_sensor", self.sample_data["calibrated_sensor_token"]
        )
        pose_rec = self.dataset.get("ego_pose", self.sample_data["ego_pose_token"])
        camera_intrinsic = np.array(cs_rec["camera_intrinsic"])

        # Get all the annotation with the specified visibilties.
        ann_recs = [
            self.dataset.get("sample_annotation", token)
            for token in self.sample["anns"]
        ]
        ann_recs = [
            ann_rec
            for ann_rec in ann_recs
            if (ann_rec["visibility_token"] in visibilities)
        ]

        im = plt.imread(f"dataset/{self.filename}")
        dpi = 80
        height, width, _ = im.shape
        figsize = width / float(dpi), height / float(dpi)
        fig, ax = plt.subplots(1, 1, figsize=figsize)

        finals_results = []
        for ann_rec in ann_recs:
            # Augment sample_annotation with token information.
            ann_rec["sample_annotation_token"] = ann_rec["token"]
            ann_rec["sample_data_token"] = self.sample["data"][self.sensor]

            # Get the box in global coordinates.
            box = self.dataset.get_box(ann_rec["token"])

            # Move them to the ego-pose frame.
            box.translate(-np.array(pose_rec["translation"]))
            box.rotate(Quaternion(pose_rec["rotation"]).inverse)

            # Move them to the calibrated sensor frame.
            box.translate(-np.array(cs_rec["translation"]))
            box.rotate(Quaternion(cs_rec["rotation"]).inverse)

            # Filter out the corners that are not in front of the calibrated sensor.
            corners_3d = box.corners()
            corners = view_points(corners_3d, camera_intrinsic, normalize=True)[:2, :]

            in_front2 = np.argwhere(corners_3d[2, :] > 0).flatten()
            corners_3d2 = corners_3d[:, in_front2]
            corner_coords = (
                view_points(corners_3d2, camera_intrinsic, True).T[:, :2].tolist()
            )

            visible = np.logical_and(corners[0, :] > 0, corners[0, :] < im.shape[1])
            visible = np.logical_and(visible, corners[1, :] < im.shape[0])
            visible = np.logical_and(visible, corners[1, :] > 0)
            visible = np.logical_and(visible, corners_3d[2, :] > 1)

            in_front = corners_3d[2, :] > 0.1

            final_vis = all(visible) and all(in_front)

            if final_vis and ann_rec["category_name"] == self.category:
                final_coords = post_process_coords(corner_coords)

                def draw_rect(selected_corners):
                    prev = selected_corners[-1]
                    for corner in selected_corners:
                        ax.plot(
                            [prev[0], corner[0]],
                            [prev[1], corner[1]],
                            color="r",
                            linewidth=2,
                        )
                        prev = corner

                for i in range(4):
                    ax.plot(
                        [corners.T[i][0], corners.T[i + 4][0]],
                        [corners.T[i][1], corners.T[i + 4][1]],
                        color="r",
                        linewidth=2,
                    )

                draw_rect(corners.T[:4])
                draw_rect(corners.T[4:])
                finals_results.append(final_coords)
        ax.imshow(im)
        plt.savefig("tmp.jpg")
        return finals_results
