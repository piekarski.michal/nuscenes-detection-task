import logging
import sys

import numpy as np
import tensorflow as tf
import tensorflow_hub as hub
from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as viz_utils
from PIL import Image
from six import BytesIO
from six.moves.urllib.request import urlopen

COCO17_HUMAN_POSE_KEYPOINTS = [
    (0, 1),
    (0, 2),
    (1, 3),
    (2, 4),
    (0, 5),
    (0, 6),
    (5, 7),
    (7, 9),
    (6, 8),
    (8, 10),
    (5, 6),
    (5, 11),
    (6, 12),
    (11, 12),
    (11, 13),
    (13, 15),
    (12, 14),
    (14, 16),
]

PATH_TO_LABELS = "S:/PycharmProjects/nuscenes-detection-task/models/research/object_detection/data/mscoco_label_map.pbtxt"
category_index = label_map_util.create_category_index_from_labelmap(
    PATH_TO_LABELS, use_display_name=True
)

tf.get_logger().setLevel("ERROR")


class Predictor:
    def __init__(self, base_model, min_score=0.9, log_level=logging.DEBUG):
        self.base_model = base_model
        self.min_score = min_score
        self.logger = logging.getLogger("tipper")
        self.logger.addHandler(logging.StreamHandler(sys.stdout))
        self.logger.setLevel(log_level)
        self.predictor = None
        self.load_predictor()
        self.image = None
        self.image_ann = None
        self.result = None

    def predict(self, img_path):
        self.load_image_into_numpy_array(img_path)
        results = self.predictor(self.image)
        self.result = {key: value.numpy() for key, value in results.items()}
        self.image_ann = self.image.copy()
        self.visualize()
        return self.image_ann[0]

    def visualize(self):
        label_id_offset = 0
        keypoints, keypoint_scores = None, None
        if "detection_keypoints" in self.result:
            keypoints = self.result["detection_keypoints"][0]
            keypoint_scores = self.result["detection_keypoint_scores"][0]

        viz_utils.visualize_boxes_and_labels_on_image_array(
            self.image_ann[0],
            self.result["detection_boxes"][0],
            (self.result["detection_classes"][0] + label_id_offset).astype(int),
            self.result["detection_scores"][0],
            category_index,
            use_normalized_coordinates=True,
            max_boxes_to_draw=200,
            min_score_thresh=self.min_score,
            agnostic_mode=False,
            keypoints=keypoints,
            keypoint_scores=keypoint_scores,
            keypoint_edges=COCO17_HUMAN_POSE_KEYPOINTS,
        )

    def load_predictor(self):
        """
        Loads model from the Tensorflow HUB
        :return:
        """

        self.logger.info(f"Loading model...")
        try:
            self.predictor = hub.load(self.base_model)
        except Exception as e:
            self.logger.error(f"Model loading failed!!! Error: {e}")
            self.predictor = None
        else:
            self.logger.info("Model loaded successfully")

    def get_results(self):
        score_threshed = self.result["detection_scores"][0] >= self.min_score
        res = np.where(score_threshed == 1)[0]
        im_size = self.image.shape
        ann = []
        for el in res:
            cat = self.result["detection_classes"][0][el]
            cat_name = category_index[cat]["name"].replace(" ", "")
            xmin = self.result["detection_boxes"][0][el][1] * im_size[2]
            ymin = self.result["detection_boxes"][0][el][0] * im_size[1]
            xmax = self.result["detection_boxes"][0][el][3] * im_size[2]
            ymax = self.result["detection_boxes"][0][el][2] * im_size[1]
            score = self.result["detection_scores"][0][el]
            ann.append((cat_name, xmin, ymin, xmax, ymax, score))
        return ann

    def load_image_into_numpy_array(self, path):
        """
        Load an image from file into a numpy array.

        Puts image into numpy array to feed into tensorflow graph.
        Note that by convention we put it into a numpy array with shape
        (height, width, channels), where channels=3 for RGB.

        Args:
          path: the file path to the image

        Returns:
          uint8 numpy array with shape (img_height, img_width, 3)
        """
        image = None
        if path.startswith("http"):
            response = urlopen(path)
            image_data = response.read()
            image_data = BytesIO(image_data)
            image = Image.open(image_data)
        else:
            image_data = tf.io.gfile.GFile(path, "rb").read()
            image = Image.open(BytesIO(image_data))

        (im_width, im_height) = image.size
        self.image = (
            np.array(image.getdata())
            .reshape((1, im_height, im_width, 3))
            .astype(np.uint8)
        )
