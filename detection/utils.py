import numpy as np


def intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)
    # return the intersection over union value
    return iou


def evaluate(gt, pred, cat="car"):
    dist = 20
    nb_predicted = len([1 for x in pred if x[0] == cat])
    nb_cars = len(gt)
    miss = abs(nb_cars - nb_predicted)
    ok = nb_cars - miss
    if ok < 0:
        ok = 0
    if nb_cars == 0:
        acc = 0
    else:
        acc = ok / nb_cars
    IOUs = []
    conf = []
    xmins = np.array([x[0] for x in gt])
    for el in pred:
        if el[0] == cat:
            conf.append(el[5])
            l = abs(xmins - el[1])
            which = np.argmin(l)
            if l[which] <= dist:
                iou = intersection_over_union([el[1], el[2], el[3], el[4]], gt[which])
                IOUs.append(iou)
    return ok, miss, acc, np.mean(IOUs), np.mean(conf)
