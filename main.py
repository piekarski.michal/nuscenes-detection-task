import cv2
import numpy as np

from detection.dataset import Dataset
from detection.detector import Predictor
from detection.utils import evaluate

IMG_PTH = "./dataset/"

# BASE_MODEL = "https://tfhub.dev/tensorflow/faster_rcnn/inception_resnet_v2_640x640/1"
BASE_MODEL = "https://tfhub.dev/tensorflow/faster_rcnn/resnet152_v1_640x640/1"
SCENE_IND = 1

def main():
    dt = Dataset(scene_ind=SCENE_IND)
    predictor = Predictor(base_model=BASE_MODEL)
    accl = []
    ioul = []
    conl = []
    while dt.sample_token != dt.last_token:
        GT = dt.iterate()
        img = predictor.predict(f"{IMG_PTH}{dt.filename}")
        ann = predictor.get_results()
        ok, miss, acc, ious, conf = evaluate(GT, ann)
        accl.append(acc)
        ioul.append(ious)
        conl.append(conf)
        cv2.namedWindow("output", cv2.WINDOW_NORMAL)
        im = cv2.imread("tmp.jpg")
        im = cv2.putText(
            im,
            f"TEST SCENE NO: {SCENE_IND} - GROUND TRUTH",
            (250, 50),
            cv2.FONT_HERSHEY_SIMPLEX,
            2,
            (255, 0, 0),
            2,
            cv2.LINE_AA,
        )
        img = cv2.putText(
            img,
            f"TEST SCENE NO: {SCENE_IND} - PREDICTED",
            (250, 50),
            cv2.FONT_HERSHEY_SIMPLEX,
            2,
            (755, 0, 0),
            2,
            cv2.LINE_AA,
        )
        img = cv2.putText(
            img,
            f"Correct: {ok}, Wrong/missed: {miss}, Accuracy: {acc * 100:.2f}%",
            (150, 810),
            cv2.FONT_HERSHEY_SIMPLEX,
            1.7,
            (255, 0, 0),
            2,
            cv2.LINE_AA,
        )
        img = cv2.putText(
            img,
            f"Mean IOU: {ious*100:.2f}%, Mean confidence: {conf*100:.2f}%",
            (150, 880),
            cv2.FONT_HERSHEY_SIMPLEX,
            1.7,
            (255, 0, 0),
            2,
            cv2.LINE_AA,
        )
        im = cv2.resize(im, (1600, 900))
        stack = np.hstack((im, img))
        cv2.imshow("output", stack)
        cv2.waitKey()
        dt.next_sample()
    cv2.destroyAllWindows()
    print(f"Mean acc {np.mean(accl)}, mean oiu {np.mean(ioul)}, mean conf {np.mean(conl)}")


if __name__ == "__main__":
    main()
